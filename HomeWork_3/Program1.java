package HomeWork_3;

import java.util.Scanner;
import java.util.Arrays;

class Program1 {
	public static int pow(int value, int powValue) {
   		if (powValue == 1) {
			return value; }
		else {
			return value * pow(value, powValue - 1); }
   	}
	public static int calculateSum (int[] a){
		int printSum = 0;
		for (int i=0; i<a.length; i++){
			printSum += a[i];
		} 
		return printSum;
	}
	public static void turnArray (int[] b){
		int returnArray[] = new int[b.length];
		for (int i = 0; i < b.length; i++) {
			int index = b.length-1-i;
			returnArray[i] = b[index];
			}
		System.out.println("Array return: " + Arrays.toString(returnArray));
	}
	public static int averageAmount (int[] c){
		int averageAm = 0;
		for (int i=0; i<c.length; i++){
			averageAm += c[i];
		}
		averageAm = averageAm/c.length;
		return averageAm;
	}
	public static void toChangeMaxMin (int[] f){
		int changeMaxMin[] = new int[f.length];
		for (int i=0; i<f.length; i++) {
			changeMaxMin[i]=f[i];
		}
		int max = 0;
		int min = 0;
		int change = 0;
		for (int i = 0; i < changeMaxMin.length; i++) {
			if (changeMaxMin[i] < changeMaxMin[min]) {
				min = i;
			}
			if (changeMaxMin[i] >= changeMaxMin[max]) {
				max = i;
				change = changeMaxMin[i];
			}
		}
		changeMaxMin[max]=changeMaxMin[min];
		changeMaxMin[min]=change;
		System.out.println("Change MaxMin: " + Arrays.toString(changeMaxMin));
	}
	public static void toBubbleSort(int g[]){
		int bubbleSort[] = new int[g.length];
		int max;
		for (int i=0; i<g.length; i++) {
			bubbleSort[i]=g[i]; 
		}
			for (int i = 0; i < bubbleSort.length; i++) 
				for (int j=1; j < bubbleSort.length-i;j++)
					if (bubbleSort[j-1] > bubbleSort[j]) {
						int temp = bubbleSort[j-1];
						bubbleSort[j-1] = bubbleSort[j];
						bubbleSort[j] = temp; 
					}
		System.out.println("BubbleSort: " + Arrays.toString(bubbleSort));
	}
	public static int toNumb (int[] d){
		int numb = 0;
		for (int i=0; i<d.length; i++){
			if (i!=d.length-1){
				int sq = d.length-1-i;
				numb += d[i]*pow(10,sq);}
				else {
					numb += d[i];
				}
		}
		return numb;
	}
	public static void main (String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the number of elements in the array:");
		int size = scanner.nextInt();
		int[] numbers = new int[size];
		System.out.println("Enter the value of the array elements:");
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = scanner.nextInt();
		}
		System.out.println("Array: " + Arrays.toString(numbers));
		System.out.println("1. Calculate sum");
		System.out.println("2. Turn array");
		System.out.println("3. Average amount");
		System.out.println("4. Change max and min");
		System.out.println("5. Bubble sort");
		System.out.println("6. To numb");
		int command = scanner.nextInt();
		
		switch(command){
		case 1:
		int result1 = calculateSum(numbers); 
		System.out.println("Amount: " + result1);
		break;
		case 2: 
		turnArray(numbers); 
		break;
		case 3:
		int result3 = averageAmount(numbers);
		System.out.println("Average amount: " + result3);
		break;
		case 4:
		toChangeMaxMin(numbers);
		break;
		case 5:
		toBubbleSort(numbers);
		break;
		case 6: 
		int result6 = toNumb(numbers);
		System.out.println("To numb: " + result6);
		break; }
		}
	}





