package HomeWork_6;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Program[] nameProgramCh1 = new Program[]{
                new Program("noname"),
                new Program("noname1"),
                new Program("noname2"),
                new Program("noname3")
        };
        Program[] nameProgramCh2 = new Program[]{
                new Program("noname4"),
                new Program("noname5"),
                new Program("noname6"),
                new Program("noname7")
        };
        Program[] nameProgramCh3 = new Program[]{
                new Program("noname8"),
                new Program("noname9"),
                new Program("noname10"),
                new Program("noname11")
        };
        Program[] nameProgramCh4 = new Program[]{
                new Program("noname12"),
                new Program("noname13"),
                new Program("noname14"),
                new Program("noname15")
        };
        Channel[] channels = new Channel[]{
                new Channel("Sts", nameProgramCh1),
                new Channel("Tnt", nameProgramCh2),
                new Channel("Efir", nameProgramCh3),
                new Channel("Dojd", nameProgramCh4)
        };
        TV tv = new TV(channels);
        RemoteController remoteController = new RemoteController(tv);
        Scanner scanner = new Scanner(System.in);
        int b = scanner.nextInt();
        if (b == 9) {
            System.out.println("Vvedite nazvanie kanala: ");
            String NewNameCh = scanner.next();
            remoteController.pressButton2(NewNameCh);
        } else {
            remoteController.pressButton(b);
        }
    }
}

