package HomeWork_6;

import java.util.Random;
public class Channel {
    String nameChannel;
    Program[] program;

    public Channel(String nameChannel, Program[] program) {
        this.nameChannel = nameChannel;
        this.program = program;
    }

    public void onClick() {
        Random random = new Random();
        int pos = random.nextInt(program.length);
        String nameProg = program[pos].getNameProgram();
        System.out.println(nameProg);
    }
}

