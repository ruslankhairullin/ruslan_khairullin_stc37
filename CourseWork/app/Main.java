package CourseWork.app;

import CourseWork.dto.StatisticDto;
import CourseWork.repositories.*;
import CourseWork.services.GameService;
import CourseWork.services.GameServiceImpl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static final String JDBC_URL = "jdbc:postgresql://localhost:5432/";
    public static final String JDBC_USER = "postgres";
    public static final String JDBC_PASSWORD = "1152";

    public static void main(String[] args) throws Exception {
//        Connection connection = DriverManager.getConnection(JDBC_URL,JDBC_USER,JDBC_PASSWORD);
//        int j=1;
        //PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
        GamesRepository gamesRepository = new GamesRepositoryListImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryListImpl();
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        Scanner scanner = new Scanner(System.in);
        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();
        Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        String shooter = first;
        String target = second;
        int i = 0;
        while (i<10) {
            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();
            int success = random.nextInt(2);
            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }
            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }
        first = scanner.nextLine();
        second = scanner.nextLine();
        gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        shooter = first;
        target = second;
        i = 0;
        while (i<10) {
            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();
            int success = random.nextInt(2);
            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }
            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }
        first = scanner.nextLine();
        second = scanner.nextLine();
        random = new Random();
        gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        shooter = first;
        target = second;
        i = 0;
        while (i<10) {
            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();
            int success = random.nextInt(2);
            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }
            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }
        StatisticDto statistic = gameService.finishGame(gameId);
        System.out.println(statistic);
        i = 10;
    }
}
