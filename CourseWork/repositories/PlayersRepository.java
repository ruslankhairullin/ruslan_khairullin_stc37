package CourseWork.repositories;
import CourseWork.models.Player;

public interface PlayersRepository {
    Player findByNickname(String nickname);
    void save (Player player);
    void update (Player player);

}
