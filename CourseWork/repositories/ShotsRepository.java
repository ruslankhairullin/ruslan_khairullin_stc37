package CourseWork.repositories;

import CourseWork.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
