package CourseWork.services;
import CourseWork.dto.StatisticDto;
import CourseWork.models.Player;
import CourseWork.models.Game;
import CourseWork.models.Shot;
import CourseWork.repositories.GamesRepository;
import CourseWork.repositories.PlayersRepository;
import CourseWork.repositories.ShotsRepository;

import java.time.LocalDateTime;

public class GameServiceImpl implements GameService {
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstNamePlayer, String secondNamePlayer) {
        Player first = checkIfExists(firstIp, firstNamePlayer);
        Player second = checkIfExists(secondIp,secondNamePlayer);
        Game game = new Game(LocalDateTime.now(),first, second, 0, 0, 0L);
        gamesRepository.save(game);
        return game.getId();
    }
    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        if (player == null) {
            player = new Player(ip, nickname,0,0,0);
            playersRepository.save(player);
        } else {
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }
    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);
        shooter.setPoints(shooter.getPoints() + 1);
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount()+1);
        }
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount()+1);
        }
        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId) {
            Game game = gamesRepository.findById(gameId);
            gamesRepository.update(game);
            Player playerOne = playersRepository.findByNickname(game.getPlayerFirst().getName());

            Player playerTwo = playersRepository.findByNickname(game.getPlayerSecond().getName());

            String playerWinner = checkGameWinner(game);
            StatisticDto statisticDto = new StatisticDto(game.getId(), playerOne.getName(), playerTwo.getName(), game.getPlayerFirstShotsCount(), game.getPlayerSecondShotsCount(), playerOne.getPoints(), playerTwo.getPoints(), playerWinner);

            return statisticDto;
        }
    private String checkGameWinner(Game game){
        if(game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()){
            Player playerOne = playersRepository.findByNickname(game.getPlayerFirst().getName());
            playerOne.setMaxWinsCount(playerOne.getMaxWinsCount() + 1);
            Player playerTwo = playersRepository.findByNickname(game.getPlayerSecond().getName());
            playerTwo.setMaxLosesCount(playerTwo.getMaxLosesCount() + 1);
            playersRepository.update(playerOne);
            playersRepository.update(playerTwo);
            return game.getPlayerFirst().getName();
        } else if(game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()){
            Player playerOne = playersRepository.findByNickname(game.getPlayerFirst().getName());
            playerOne.setMaxLosesCount(playerOne.getMaxLosesCount() + 1);
            Player playerTwo = playersRepository.findByNickname(game.getPlayerSecond().getName());
            playerTwo.setMaxWinsCount(playerTwo.getMaxWinsCount() + 1);
            playersRepository.update(playerOne);
            playersRepository.update(playerTwo);
            return game.getPlayerSecond().getName();
        } else
            return "Ничья";
    }
}
