package CourseWork.services;

import CourseWork.dto.StatisticDto;

public interface GameService {
    Long startGame(String firstIp, String secondIp, String firstNamePlayer, String secondNamePlayer);
    void shot (Long gameId, String shooterNickname, String targetNickname);
    StatisticDto finishGame(Long gameId);
}
