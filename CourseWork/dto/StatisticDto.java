package CourseWork.dto;

public class StatisticDto {
    private Long gameId;
    private String firstPlayerNickname;
    private String secondPlayerNickname;
    private Integer shotsCountFromFirst;
    private Integer shotsCountFromSecond;
    private Integer firstPlayerScore;
    private Integer secondPlayerScore;
    private String playerWinner;

    public StatisticDto(Long gameId, String firstPlayer, String secondPlayer, Integer shotsCountFromFirst, Integer shotsCountFromSecond, Integer firstPlayerScore, Integer secondPlayerScore, String playerWinner) {
        this.gameId = gameId;
        this.firstPlayerNickname = firstPlayer;
        this.secondPlayerNickname = secondPlayer;
        this.shotsCountFromFirst = shotsCountFromFirst;
        this.shotsCountFromSecond = shotsCountFromSecond;
        this.firstPlayerScore = firstPlayerScore;
        this.secondPlayerScore = secondPlayerScore;
        this.playerWinner = playerWinner;
    }



    @Override
    public String toString() {
        return
                "Игра с ID = " + gameId + "\n" +
                        "Игрок 1: " + firstPlayerNickname + ", попаданий - " + shotsCountFromFirst + ", всего очков - " + firstPlayerScore + "\n" +
                        "Игрок 2: " + secondPlayerNickname + ", попаданий - " + shotsCountFromSecond + ", всего очков - " + secondPlayerScore + "\n" +
                        "Победа: " + playerWinner + "\n" +
                        "Игра длилась: " + " секунд";
    }
}
