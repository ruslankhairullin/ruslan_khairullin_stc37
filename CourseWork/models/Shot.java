package CourseWork.models;

import java.time.LocalDateTime;
import java.util.Objects;

public class Shot {
    private Long id;
    private LocalDateTime dateTime;
    private Game game;
    private Player shooter;
    private Player target;

    public Shot(LocalDateTime dateTime, Game game, Player shooter, Player target) {
        this.dateTime = dateTime;
        this.game = game;
        this.shooter = shooter;
        this.target = target;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getShooter() {
        return shooter;
    }

    public void setShooter(Player shooter) {
        this.shooter = shooter;
    }

    public Player getTarget() {
        return target;
    }

    public void setTarget(Player target) {
        this.target = target;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Objects.equals(dateTime, shot.dateTime) && Objects.equals(game, shot.game) && Objects.equals(shooter, shot.shooter) && Objects.equals(target, shot.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dateTime, game, shooter, target);
    }

    @Override
    public String toString() {
        return "Shot{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", game=" + game.getId() +
                ", shooter=" + shooter.getName() +
                ", target=" + target.getName() +
                '}';
    }
}
