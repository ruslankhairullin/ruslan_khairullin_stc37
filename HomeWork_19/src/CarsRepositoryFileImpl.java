package HomeWork_19.src;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.List;

public class CarsRepositoryFileImpl implements CarsRepository {
    private String filename;

    public CarsRepositoryFileImpl(String filename) {
        this.filename = filename;
    }

    @Override
    public List<Cars> findAll() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filename));
            List<Cars> cars = reader
                    .lines()
                    .map(line -> {
                        String parsedLine[] = line.split("\\W+");
                        return new Cars(Integer.parseInt(parsedLine[1]), parsedLine[2], parsedLine[3],
                                Integer.parseInt(parsedLine[4]), Integer.parseInt(parsedLine[5]));
                    }).collect(Collectors.toList());
            return cars;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Cars> filterForColor(String color, int mileage) {
        List<Cars> carsFilter = findAll();
        carsFilter.stream()
                .filter(filter -> filter.getColor().equals(color) || filter.getMileage() == mileage)
                .map(Cars::getNumber)
                .forEach(entity -> System.out.print(entity + " "));
        return carsFilter;
    }
    @Override
    public void filterForPrice(int lowPrice, int highPrice) {
        System.out.println();
        int k = findAll().stream()
                .filter(filter -> filter.getPrice() >= lowPrice && filter.getPrice() <= highPrice)
                .map(Cars::getModel)
                .collect(Collectors.toSet())
                .size();
        System.out.println(k);
    }
    @Override
    public void colorOflowPriceCar() {
        List<Cars> colorOfCar = findAll();
        colorOfCar.stream()
                .sorted(Comparator.comparingInt(Cars::getPrice))
                .filter(filter -> filter.getNumber() == 1)
                .map(Cars::getColor)
                .forEach(System.out::println);
    }

    @Override
    public void averagePriceForCamry() {
        List<Cars> averagePrice = findAll();
        System.out.println(averagePrice.stream()
                .filter(filter -> filter.getModel().equals("CAMRY"))
                .map(Cars::getPrice)
                .mapToInt(Integer::intValue)
                .average()
                .getAsDouble());
    }
}
