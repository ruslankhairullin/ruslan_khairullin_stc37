package HomeWork_19.src;
import java.util.List;
public interface CarsRepository {
    List<Cars> findAll();
    List<Cars> filterForColor(String color, int mileage);
    void filterForPrice(int lowPrice, int highPrice);
    void colorOflowPriceCar();
    void averagePriceForCamry();
}
