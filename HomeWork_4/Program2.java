package HomeWork_4;

import java.util.Scanner;

class Program2 {
	public static boolean recursion(int array[], int element, int left, int right) {
			boolean isExist = false;
			int middle =  left + (right - left) / 2;
			if (left <= right){
				if (array[middle] < element) {
					return recursion(array, element, middle + 1, right);
				} else if (array[middle] > element) {
					return recursion(array, element, left, middle - 1);
				} else if (array[middle]==element) {
					isExist = true;

				}
			}
		return isExist;
    }
    public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numberForSearch = scanner.nextInt();
		int numbers[] = {1, 7, 22, 28, 31, 33, 43, 55, 87, 99};
		int left = 0;
		int right = numbers.length - 1;
		boolean result = recursion(numbers,numberForSearch,left,right);
		System.out.println(result);
	}
}
