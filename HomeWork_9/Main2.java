package HomeWork_9;
import java.util.Arrays;

public class Main2 {
    public static void main(String[] args) {
        int[] nums = {123043, 560783, 980763};
        String[] lines = {"Slovo1", "slovo2", "slovo3"};
        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(nums, lines);
        NumbersProcess numbersProcess1 = number -> {
            int result = 0;
            while (number>0){
                result = (result*10)+(number%10);
                number /= 10;
        }
            return result;
        };
        NumbersProcess numbersProcess2 = number -> {
            int result = 0;
            int pow = 1;
            while (number > 0) {
                int k = number % 10;
                if (k != 0) {
                    result = result + (k * pow);
                    pow *= 10;
                }
                number /= 10;
            }
            return result;
        };
        NumbersProcess numbersProcess3 = number -> {
            int result = 0;
            int pow = 1;
            while (number > 0) {
                int k = number % 10;
                if (k % 2 != 0 || k==0) {
                    result = result + ((k+1) * pow);
                    pow *= 10;
                } else {
                    result = result + (k * pow);
                    pow *= 10;
                }
                number /= 10;
            }
            return result;
        };
        StringProcess stringProcess1 = word -> new StringBuilder(word).reverse().toString();

        StringProcess stringProcess2 = word -> {
            StringBuilder temporaryString = new StringBuilder();
            int index = 0;
            while (index < word.length()) {
                char tempChar = word.charAt(index);
                if (!Character.isDigit(tempChar)){
                    temporaryString.append(tempChar);
                }
                index++;
            }
            return temporaryString.toString();
        };
        StringProcess stringProcess3 = word -> {
            StringBuilder temporaryString = new StringBuilder();
            int index = 0;
            while (index < word.length()) {
                char tempChar = word.charAt(index);
                if (Character.isLowerCase(tempChar)) {
                    tempChar = Character.toUpperCase(tempChar);
                }
                temporaryString.append(tempChar);
                index++;
            }
            return temporaryString.toString();
        };


        int[] outputArray1 = numbersAndStringProcessor.processInt(numbersProcess1);
        int[] outputArray2 = numbersAndStringProcessor.processInt(numbersProcess2);
        int[] outputArray3 = numbersAndStringProcessor.processInt(numbersProcess3);

        String[] outputArray4 = numbersAndStringProcessor.processString(stringProcess1);
        String[] outputArray5 = numbersAndStringProcessor.processString(stringProcess2);
        String[] outputArray6 = numbersAndStringProcessor.processString(stringProcess3);


        System.out.println(Arrays.toString(outputArray1));
        System.out.println(Arrays.toString(outputArray2));
        System.out.println(Arrays.toString(outputArray3));

        System.out.println(Arrays.toString(outputArray4));
        System.out.println(Arrays.toString(outputArray5));
        System.out.println(Arrays.toString(outputArray6));

    }
}
