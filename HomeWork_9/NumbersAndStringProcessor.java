package HomeWork_9;

public class NumbersAndStringProcessor {
    int numbersInt[];
    String wordsString[];

    public NumbersAndStringProcessor(int[] numbersInt, String[] wordsString) {
        this.numbersInt = numbersInt;
        this.wordsString = wordsString;
    }
    public int[] processInt(NumbersProcess numbersProcess) {
        int[] outputArray = new int[numbersInt.length];
        for (int i = 0; i<numbersInt.length; i++){
            outputArray[i] = numbersProcess.process(numbersInt[i]);
        }
        return outputArray;
    }
    public String[] processString(StringProcess stringProcess) {
        String[] resultString = new String[wordsString.length];
        for (int i=0; i<this.wordsString.length; i++) {
            resultString[i] = stringProcess.process(wordsString[i]);
        }
        return resultString;
    }


}
