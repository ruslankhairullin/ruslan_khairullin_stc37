package HomeWork_2;

import java.util.Scanner;
import java.util.Arrays;

class Program5 {
	public static void main (String args[]) {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int numbers[] = new int[size];
		boolean isSorted = false;
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = scanner.nextInt();
		}
		while (!isSorted) {
			isSorted = true;
			for (int i = 0; i < numbers.length-1; i++) {
					if (numbers[i] > numbers[i+1]) {
						isSorted = false;
						int max = numbers[i];
						numbers[i] = numbers[i+1];
						numbers[i+1] = max; 
					}
			}
		}
		System.out.println(Arrays.toString(numbers));
	}
}
