package HomeWork_2;

import java.util.Scanner;
import java.util.Arrays;

class Program4 {
	public static void main (String args[]) {
		Scanner scanner = new Scanner(System.in);
		int size = scanner.nextInt();
		int numbers[] = new int[size];
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = scanner.nextInt();
		}
		int max = 0;
		int min = 0;
		int change = 0;
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] < numbers[min]) {
				min = i;
				}
			if (numbers[i] > numbers[max]) {
				max = i;
				change = numbers[i];
				}
		}
		numbers[max]=numbers[min];
		numbers[min]=change;
		System.out.println(Arrays.toString(numbers));
		}
}