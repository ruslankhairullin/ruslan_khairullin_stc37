package HomeWork_8;

public abstract class GeometricFigures implements Scalable {
    protected int a;
    protected int b;
    protected int x;
    protected int y;
    public GeometricFigures(int a, int b,int x, int y) {
        if (a>0) {
            this.a = a;
        }
        if (b>0) {
            this.b = b;
        }
        if (x>0) {
            this.x = x;
        }
        if (y>0) {
            this.y = y;
        }
    }
    public void toScale(int k) {
        a = a*k;
        b=b*k;
    }
    public void toMove (int i){
        x = x+i;
        y = y+i;
    }

    public abstract int perimeter();
    public abstract int area();

}
