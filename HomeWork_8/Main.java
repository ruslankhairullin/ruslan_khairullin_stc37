package HomeWork_8;

public class Main {

    public static void main(String[] args) {
	GeometricFigures myCoolSquare = new Square(5,1,2);
	GeometricFigures rectangle = new Rectangle(3,4,3,4);
	GeometricFigures ellipse = new Ellipse(5,4,5,6);
	GeometricFigures circle = new Circle(5,7,8);
	myCoolSquare.toScale(2);
	myCoolSquare.toMove(1);
        System.out.println("Площадь квадрата: " + myCoolSquare.area() + ", координаты:" + " (" + myCoolSquare.x + ";" + myCoolSquare.y + ")");
        System.out.println("Периметр квадрата: " + myCoolSquare.perimeter() + ", координаты:" + " (" + myCoolSquare.x + ";" + myCoolSquare.y + ")");
		System.out.println("Площадь прямоугольника: " + rectangle.area() + ", координаты:" + " (" + rectangle.x + ";" + rectangle.y + ")");
		System.out.println("Периметр прямоугольника: " + rectangle.perimeter() + ", координаты:" + " (" + rectangle.x + ";" + rectangle.y + ")");
		System.out.println("Площадь эллипса: " + ellipse.area() + ", координаты:" + " (" + ellipse.x + ";" + ellipse.y + ")");
		System.out.println("Периметр эллипса: " + ellipse.perimeter() + ", координаты:" + " (" + ellipse.x + ";" + ellipse.y + ")");
		System.out.println("Площадь круга: " + circle.area() + ", координаты:" + " (" + circle.x + ";" + circle.y + ")");
		System.out.println("Периметр круга: " + circle.perimeter() + ", координаты:" + " (" + circle.x + ";" + circle.y + ")");
    }
}
