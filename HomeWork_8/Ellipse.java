package HomeWork_8;

import java.lang.Math;
public class Ellipse extends GeometricFigures{
    public Ellipse(int a,int b,int x, int y) {
        super(a,b,x,y);

    }
    public int area() {
        return (int) (Math.PI * (a * b));
    }
    public int perimeter(){
        return (int) (2*Math.PI*(Math.sqrt((a*a+b*b)/2)));
    }

    @Override
    public void movable(int i) {

    }
}
