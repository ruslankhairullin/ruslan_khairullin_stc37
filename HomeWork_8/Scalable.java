package HomeWork_8;

public interface Scalable extends Movable {
    void toScale(int k);
}
