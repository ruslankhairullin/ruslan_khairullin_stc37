package HomeWork_7.src;

public class Main {

    public static void main(String[] args) {
        Builder builder = new Builder.builder()
                .firstName("Руслан")
                .lastName("Хайруллин")
                .age(23)
                .isWorker(true)
                .build();
        builder.newUser();
    }
}
