package HomeWork_7.src;
public class Builder {
    private String firstName = " ";
    private String lastName = " ";
    private int age = 0;
    private boolean isWorker;

    public static class builder {
        private Builder builderUser;
        public builder(){
            this.builderUser = new Builder();
        }

        public builder firstName(String name) {
            builderUser.firstName = name;
            return this;
        }
        public builder lastName(String name) {
            builderUser.lastName = name;
            return this;
        }
        public builder age(int age) {
            builderUser.age = age;
            return this;
        }
        public builder isWorker(boolean isWorker) {
            builderUser.isWorker = isWorker;
            return this;
        }
        public Builder build() {
            return builderUser;
        }
    }
    public void newUser() {
        System.out.println("FirstName: " + firstName + "\n" + "LastName: " + lastName + "\n" +
                "Age: " + age + "\n" + "isWorking: " + isWorker);
    }


}
