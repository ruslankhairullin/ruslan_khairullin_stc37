package HomeWork_11.src;
public class Main {

    public static void main(String[] args) {
        InnoList list = new InnoArrayList();
        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);
//        InnoIterator iterator = list.iterator();
//        while (iterator.hasNext()) {
//            System.out.println(iterator.next() + " ");
//        }
        InnoList list2 = new InnoLinkedList();
        list2.add(7);
        list2.add(8);
        list2.add(10);
        list2.add(12);
        list2.add(15);
        list2.add(20);
        list2.add(-77);
        list2.add(101);
        list2.remove(123);
        InnoIterator iterator2 = list2.iterator();
        while (iterator2.hasNext()) {
            System.out.println(iterator2.next() + " ");
        }

    }
}
