/**
 * 11.03.2021
 * 24. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
package HomeWork_11.src;
// Плюсы - неограниченный размер (ограничен VM), быстрое добавление/удаление в начало/конец
// Минусы - медленный доступ по  индексу
public class InnoLinkedList implements InnoList {

    private static class Node {
        int value;
        Node next;
        Node previous;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;

    private int count;

    // get(7)
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            current.value = element;
        } else {
            System.out.println("Нет такого индекса!");
        }
    }
    @Override
    public void addToBegin(int element) {
        Node tempNode = new Node(element);
        tempNode.next = first;
        first = tempNode;
    }

    @Override
    public void removeByIndex(int index) {
        if (index >= 1 && index <= this.size()) {
            Node current = first;
            Node previous = first;
            int i = 0;
            while (index > i) {
                previous = current;
                current = current.next;
                i++;
            }
            previous.next = current.next;
        } else if (index == 0) {
            first = first.next;
        } else System.out.println("Такого индекса нет!");
    }

//    @Override
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        // если список пустой
//        if (first == null) {
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        } else {
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null) {
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли по последнего узла
//            // теперь новый узел - самый последний (следующий после предыдущего последнего)
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;

        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {
        if (contains(element) == true) {
            Node current = first;
            Node previous = first;
            while (current.value != element) {
                previous = current;
                current = current.next;
            }
            previous.next = current.next;
        }
        else {
            System.out.println("Нет такого элемента!");
        }
    }

    @Override
    public boolean contains(int element) {
            Node current = first;
            try {
                while (current.value != element) {
                current = current.next;
            } return true; }
                catch (NullPointerException e){
                return false;
             }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current.next != null;
        }
    }
}
